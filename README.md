<p align="center">
  <a align="center">Zemoga Test</a>
</p>

## Contents

- [Contents](#contents)
- [About app](#about-app)
- [Architecture](#architecture)
- [Conclusions](#conclusions)

 ## About app

* To start with, you can run the application  clone the reposiroty and going to the main.dart file, in the path zemoga_test\lib\main.dart, on funcion main.

* I used Provider as state managment to cotroll the data and models of the application.

* This application was made in version 3.0.5 of flutter and 2.17.6 of Dart

* The Class Adapt makes the application 100% responsive.

* When the application starts, it consults the api to obtain the posts, once it has been done, the data is saved in the shared preferences and thus they are in charge of saving them in the app cache.

* The third party libraries used in this application were:

    - http: ^0.13.5 A composable, Future-based library for making HTTP requests. This package contains a set of high-level functions and classes that make it easy to  consume HTTP resources. It's multi-platform, and supports mobile, desktop, and the browser.

    - intl: ^0.17.0 Provides internationalization and localization facilities, including message translation, plurals and genders, date/number formatting and parsing, and bidirectional text.

    - provider: ^6.0.3 Provider, is a package that has been recently released (in Google I/O) which has the function of allowing us to manage the state of our app.

    - shared_preferences: ^2.0.15 Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.). Data may be persisted to disk asynchronously, and there is no guarantee that writes will be persisted to disk after returning, so this plugin must not be used for storing critical data.

    - flutter_launcher_icons: A command-line tool which simplifies the task of updating your Flutter app's launcher icon. Fully flexible, allowing you to choose what platform you wish to update the launcher icon for and if you want, the option to keep your old launcher icon in case you want to revert back sometime in the future.

    - flutter_icons: Customizable Icons for Flutter,Inspired by react-native-vector-icons

## Architecture

The architecture proposed by me for this application is based on MVC MODEL-VIEW-CONTROLLER, because this app has little scalability and the navigation and processes are not so big and complicated.
## Conclusions

who may be interested.

This application is the result of a hard effort and of which I am very proud. I want to thank you for having taken me into account regardless of whether I am in the selection process. They made me realize all the capabilities and qualities I have.

Felipe Osorio.