import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia = PreferenciasUsuario.internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario.internal();

  late SharedPreferences _prefs;

  iniciarPreferencias() async {
    _prefs = await SharedPreferences.getInstance();
  }

  limpiar() async {
    _prefs.clear();
  }

  String get postList {
    return _prefs.getString('postList') ?? '';
  }

  set postList(String valor) {
    _prefs.setString('postList', valor);
  }
}
