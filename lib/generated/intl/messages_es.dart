// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alert": MessageLookupByLibrary.simpleMessage("Alerta!"),
        "all": MessageLookupByLibrary.simpleMessage("Todos"),
        "cancel": MessageLookupByLibrary.simpleMessage("CANCELAR"),
        "comments": MessageLookupByLibrary.simpleMessage("Comentarios"),
        "confirm": MessageLookupByLibrary.simpleMessage(
            "¿Usted está seguro de quiere eliminar todos las publicaciones?"),
        "description": MessageLookupByLibrary.simpleMessage("Descripción"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "favorites": MessageLookupByLibrary.simpleMessage("Favoritos"),
        "loadApi": MessageLookupByLibrary.simpleMessage(
            "Presiona el boton para cargar los datos"),
        "name": MessageLookupByLibrary.simpleMessage("Nombre"),
        "noFavoritePosts": MessageLookupByLibrary.simpleMessage(
            "No hay publicaciones favoritas"),
        "phone": MessageLookupByLibrary.simpleMessage("Teléfono"),
        "post": MessageLookupByLibrary.simpleMessage("Publicación"),
        "posts": MessageLookupByLibrary.simpleMessage("Publicaciones"),
        "title": MessageLookupByLibrary.simpleMessage("Título"),
        "user": MessageLookupByLibrary.simpleMessage("Usuario"),
        "website": MessageLookupByLibrary.simpleMessage("Sitio web")
      };
}
