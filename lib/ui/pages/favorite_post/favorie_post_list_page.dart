import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../core/viewmodels/post_list_page_model.dart';
import '../../../generated/l10n.dart';
import '../../responsive/Adapt.dart';
import '../../widgets/favorite_icon_widget.dart';

class FavoritePostListPage extends StatefulWidget {
  const FavoritePostListPage({Key? key}) : super(key: key);

  @override
  State<FavoritePostListPage> createState() => _FavoritePostListPageState();
}

class _FavoritePostListPageState extends State<FavoritePostListPage> {
  PostListPageProvider? postListPageProvider;
  final space = Container(
    width: Adapt.wp(0.5),
  );
  @override
  Widget build(BuildContext context) {
    postListPageProvider =
        Provider.of<PostListPageProvider?>(context, listen: true);

    return Scaffold(
      body: Center(
          child: postListPageProvider!.postFavoriteModelList.isNotEmpty
              ? SizedBox(
                  height: Adapt.hp(80),
                  width: Adapt.screenW(),
                  child: ListView.separated(
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () async {
                        postListPageProvider!.navigationToPostListDetailPage(
                            postListPageProvider!.postModelList![index],
                            context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          space,
                          Container(
                            alignment: Alignment.centerLeft,
                            height: Adapt.hp(10),
                            width: Adapt.wp(80),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: Adapt.px(5), right: Adapt.px(5)),
                              child: Text(
                                postListPageProvider!
                                    .postFavoriteModelList[index].title,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: Adapt.px(25),
                                ),
                              ),
                            ),
                          ),
                          FavoriteIcon(
                            postModel: postListPageProvider!
                                .postFavoriteModelList[index],
                          ),
                          space
                        ],
                      ),
                    ),
                    separatorBuilder: (context, index) => Container(
                      height: Adapt.hp(0.15),
                      color: Colors.grey,
                    ),
                    itemCount:
                        postListPageProvider!.postFavoriteModelList.length,
                  ),
                )
              : Container(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.only(left: Adapt.px(20)),
                    child: Text(
                      S.current.noFavoritePosts,
                      style: TextStyle(
                        fontSize: Adapt.px(30),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )),
    );
  }
}
