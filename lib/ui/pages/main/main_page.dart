import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zemoga_test/ui/pages/post/post_list_page.dart';
import 'package:zemoga_test/ui/responsive/Adapt.dart';

import '../../../core/viewmodels/post_detail_page_model.dart';
import '../../../core/viewmodels/post_list_page_model.dart';
import '../../../generated/l10n.dart';
import '../favorite_post/favorie_post_list_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  PostListPageProvider? postListPageProvider;
  PostDetailPageProvider? userListPageProvider;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      postListPageProvider =
          Provider.of<PostListPageProvider>(context, listen: false);
      postListPageProvider!.postReady
          ? null
          : postListPageProvider!.getPostList();
      userListPageProvider =
          Provider.of<PostDetailPageProvider>(context, listen: false);
      userListPageProvider!.getUserList();
    });
  }

  @override
  Widget build(BuildContext context) {
    postListPageProvider =
        Provider.of<PostListPageProvider>(context, listen: true);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              S.current.posts,
              style: TextStyle(
                fontSize: Adapt.px(40),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          bottom: TabBar(
            tabs: [
              Tab(text: S.current.all),
              Tab(text: S.current.favorites),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            PostListPage(),
            FavoritePostListPage(),
          ],
        ),
        floatingActionButton: postListPageProvider!.postModelList!.isNotEmpty
            ? FloatingActionButton(
                backgroundColor: Colors.red,
                child: Icon(
                  Icons.delete,
                  size: Adapt.px(40),
                ),
                onPressed: () {
                  postListPageProvider =
                      Provider.of<PostListPageProvider>(context, listen: false);

                  postListPageProvider!.cleanPost();
                },
              )
            : FloatingActionButton(
                backgroundColor: Colors.green,
                child: Icon(
                  Icons.refresh_outlined,
                  size: Adapt.px(50),
                ),
                onPressed: () {
                  postListPageProvider =
                      Provider.of<PostListPageProvider>(context, listen: false);
                  postListPageProvider!.getPostList();
                },
              ),
      ),
    );
  }
}
