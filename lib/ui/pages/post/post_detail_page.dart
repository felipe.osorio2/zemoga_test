import 'package:flutter/material.dart';
import 'package:zemoga_test/ui/widgets/favorite_icon_widget.dart';

import '../../../core/models/comment_model.dart';
import '../../../core/models/post_model.dart';
import '../../../core/models/user_model.dart';
import '../../../generated/l10n.dart';
import '../../responsive/Adapt.dart';
import '../../widgets/info_user_text_widget.dart';
import '../../widgets/titlle_text_widget.dart';

// ignore: must_be_immutable
class PostDetailPage extends StatelessWidget {
  PostDetailPage(
      {super.key,
      required this.postModel,
      required this.userModel,
      required this.listCommentModel});
  final spaceHeight = SizedBox(
    height: Adapt.hp(1),
  );
  List<CommentModel>? listCommentModel;
  PostModel postModel;
  UserModel? userModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            S.current.post,
            style: TextStyle(
              fontSize: Adapt.px(40),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        actions: [
          SizedBox(
            width: Adapt.wp(15),
            child: FavoriteIcon(
              postModel: postModel,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: SizedBox(
              width: Adapt.screenW(),
              child: Column(
                children: [
                  spaceHeight,
                  TittleText(text: S.current.title),
                  spaceHeight,
                  Container(
                    height: Adapt.hp(6),
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: Adapt.px(20)),
                      child: Text(
                        postModel.title,
                        style: TextStyle(
                          fontSize: Adapt.px(25),
                        ),
                      ),
                    ),
                  ),
                  spaceHeight,
                  spaceHeight,
                  TittleText(text: S.current.description),
                  Container(
                    height: Adapt.hp(18),
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: Adapt.px(20)),
                      child: Text(
                        postModel.body,
                        style: TextStyle(
                          fontSize: Adapt.px(25),
                        ),
                      ),
                    ),
                  ),
                  spaceHeight,
                  TittleText(text: S.current.user),
                  spaceHeight,
                  InfoUserText(text: '${S.current.name}: ${userModel?.name}'),
                  InfoUserText(text: '${S.current.email}: ${userModel?.email}'),
                  InfoUserText(text: '${S.current.phone}: ${userModel?.phone}'),
                  InfoUserText(
                      text: '${S.current.website}: ${userModel?.website}'),
                  spaceHeight,
                  spaceHeight,
                  Container(
                    height: Adapt.hp(6),
                    color: const Color.fromARGB(255, 207, 205, 205),
                    alignment: Alignment.centerLeft,
                    child: TittleText(text: S.current.comments),
                  ),
                  spaceHeight,
                  listViewOfComments(),
                ],
              )),
        ),
      ),
    );
  }

  Widget listViewOfComments() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            alignment: Alignment.centerLeft,
            height: Adapt.hp(22),
            width: Adapt.wp(80),
            child: Padding(
              padding: EdgeInsets.only(left: Adapt.px(10), right: Adapt.px(10)),
              child: Text(
                listCommentModel![index].body,
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: Adapt.px(24),
                ),
              ),
            ),
          ),
        ],
      ),
      separatorBuilder: (context, index) => Container(
        height: Adapt.hp(0.15),
        color: Colors.grey,
      ),
      itemCount: listCommentModel!.length,
    );
  }
}
