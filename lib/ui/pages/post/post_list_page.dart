import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zemoga_test/ui/widgets/alert_dialog_widget.dart';
import '../../../core/viewmodels/post_detail_page_model.dart';
import '../../../core/viewmodels/post_list_page_model.dart';
import '../../../generated/l10n.dart';
import '../../responsive/Adapt.dart';
import '../../widgets/favorite_icon_widget.dart';

class PostListPage extends StatefulWidget {
  const PostListPage({Key? key}) : super(key: key);

  @override
  State<PostListPage> createState() => _PostListPageState();
}

class _PostListPageState extends State<PostListPage> {
  PostListPageProvider? postListPageProvider;
  PostDetailPageProvider? userProvider;
  final space = Container(
    width: Adapt.wp(0.5),
  );
  @override
  Widget build(BuildContext context) {
    postListPageProvider =
        Provider.of<PostListPageProvider?>(context, listen: true);
    userProvider = Provider.of<PostDetailPageProvider>(context, listen: true);

    return Scaffold(
      body: Center(
        child: postListPageProvider!.postReady
            ? postListPageProvider!.postModelList!.isNotEmpty
                ? SizedBox(
                    height: Adapt.hp(80),
                    width: Adapt.screenW(),
                    child: ListView.separated(
                      itemCount: postListPageProvider!.postModelList!.length,
                      separatorBuilder: (context, index) => Container(
                        height: Adapt.hp(0.15),
                        color: Colors.grey,
                      ),
                      itemBuilder: (context, index) => Dismissible(
                        key: UniqueKey(),
                        background: Container(
                          color: Colors.red,
                          child: Row(
                            children: [
                              SizedBox(width: Adapt.wp(5)),
                              const Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                              const Spacer(),
                              const Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                              SizedBox(width: Adapt.wp(5)),
                            ],
                          ),
                        ),
                        onDismissed: (direction) {
                          postListPageProvider!.postModelList!.removeAt(index);
                        },
                        confirmDismiss: (direction) => showDialog(
                          context: context,
                          builder: (context) => AlertDialogW(
                              titlle: S.current.alert,
                              content: S.current.confirm),
                        ),
                        child: GestureDetector(
                            onTap: () async {
                              postListPageProvider!
                                  .navigationToPostListDetailPage(
                                      postListPageProvider!
                                          .postModelList![index],
                                      context);
                            },
                            child: listPostWidget(space,
                                postListPageProvider!.postModelList![index])),
                      ),
                    ),
                  )
                : Container(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.only(left: Adapt.px(20)),
                      child: Text(
                        S.current.loadApi,
                        style: TextStyle(
                          fontSize: Adapt.px(30),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
            : Container(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.only(left: Adapt.px(20)),
                  child: Text(
                    S.current.loading,
                    style: TextStyle(
                      fontSize: Adapt.px(30),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}

Widget listPostWidget(space, postModel) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      space,
      Container(
        alignment: Alignment.centerLeft,
        height: Adapt.hp(10),
        width: Adapt.wp(80),
        child: Padding(
          padding: EdgeInsets.only(left: Adapt.px(5), right: Adapt.px(5)),
          child: Text(
            postModel.title,
            textAlign: TextAlign.start,
            style: TextStyle(
              fontSize: Adapt.px(25),
            ),
          ),
        ),
      ),
      FavoriteIcon(
        postModel: postModel,
      ),
      space
    ],
  );
}
