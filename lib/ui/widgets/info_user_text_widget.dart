import 'package:flutter/material.dart';

import '../responsive/Adapt.dart';

class InfoUserText extends StatelessWidget {
  final String text;
  const InfoUserText({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Adapt.hp(3),
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.only(left: Adapt.px(20)),
        child: Text(
          text,
          style: TextStyle(
            fontSize: Adapt.px(25),
          ),
        ),
      ),
    );
  }
}
