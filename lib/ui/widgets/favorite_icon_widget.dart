// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/models/post_model.dart';
import '../../core/viewmodels/post_list_page_model.dart';
import '../responsive/Adapt.dart';

class FavoriteIcon extends StatelessWidget {
  PostListPageProvider? postListPageProvider;
  PostModel postModel;
  FavoriteIcon({super.key, required this.postModel});

  @override
  Widget build(BuildContext context) {
    postListPageProvider =
        Provider.of<PostListPageProvider?>(context, listen: true);
    return postModel.favorite
        ? GestureDetector(
            onTap: () {
              postListPageProvider!.changeFavoriteFalse(postModel);
            },
            child: Icon(
              Icons.star,
              color: Colors.amber,
              size: Adapt.px(40),
            ),
          )
        : GestureDetector(
            onTap: () {
              postListPageProvider!.changeFavoriteTrue(postModel);
            },
            child: Icon(
              Icons.star_border,
              size: Adapt.px(40),
              color: Colors.amber,
            ),
          );
  }
}
