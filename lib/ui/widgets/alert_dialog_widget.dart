import 'package:flutter/material.dart';

import '../../generated/l10n.dart';
import '../responsive/Adapt.dart';

// ignore: must_be_immutable
class AlertDialogW extends StatelessWidget {
  String titlle;
  String content;
  AlertDialogW({super.key, required this.titlle, required this.content});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text(titlle)),
      content: Text(
        content,
        textAlign: TextAlign.center,
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: Adapt.wp(32),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(Adapt.px(10)),
                  ),
                ),
                child: Text(
                  S.current.cancel.toUpperCase(),
                  style: TextStyle(
                    fontSize: Adapt.px(25),
                  ),
                ),
                onPressed: () => Navigator.of(context).pop(false),
              ),
            ),
            Container(
              width: Adapt.wp(5),
            ),
            SizedBox(
              width: Adapt.wp(32),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(Adapt.px(10)),
                  ),
                ),
                child: Text(
                  'OK',
                  style: TextStyle(
                    fontSize: Adapt.px(25),
                  ),
                ),
                onPressed: () => Navigator.of(context).pop(true),
              ),
            )
          ],
        ),
      ],
    );
  }
}
