import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:zemoga_test/generated/l10n.dart';
import 'package:zemoga_test/preferences/preferences_user.dart';
import 'package:zemoga_test/ui/pages/main/main_page.dart';
import 'package:zemoga_test/ui/theme/app_themes.dart';

import 'core/viewmodels/post_detail_page_model.dart';
import 'core/viewmodels/post_list_page_model.dart';

void main() async {
  final prefs = PreferenciasUsuario();
  WidgetsFlutterBinding();
  WidgetsFlutterBinding.ensureInitialized();
  await prefs.iniciarPreferencias();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => PostListPageProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => PostDetailPageProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Material App',
        debugShowCheckedModeBanner: false,
        theme: AppThemes.themeData,
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
        home: const MainPage(),
      ),
    );
  }
}
