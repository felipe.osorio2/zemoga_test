class Env {
  static const String _posts = "https://jsonplaceholder.typicode.com/posts";
  static get posts => _posts;

  static const String _users = "https://jsonplaceholder.typicode.com/users";
  static get users => _users;

  static const String _comment =
      "https://jsonplaceholder.typicode.com/comments";
  static get comment => _comment;
}
