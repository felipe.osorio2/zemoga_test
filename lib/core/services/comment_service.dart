import 'package:http/http.dart' as http;

import 'dart:convert';
import '../enviroment/environment.dart';

import '../models/comment_model.dart';

class CommentService {
  Future<List<CommentModel>>? getCommentList(postId) async {
    List<CommentModel> postModelList = [];
    String url = Env.comment;

    final res = await http.get(
      Uri.parse(url),
    );

    dynamic commentJson = json.decode(res.body);

    for (var i = 0; i < commentJson.length; i++) {
      CommentModel commentModel = CommentModel.fromJson(commentJson[i]);
      if (commentModel.postId == postId) {
        postModelList.add(commentModel);
      }
    }

    return postModelList;
  }
}
