import 'package:http/http.dart' as http;

import 'dart:convert';
import '../enviroment/environment.dart';

import '../models/post_model.dart';

class PostService {
  Future<List<PostModel>>? getPostList() async {
    List<PostModel> postModelList = [];
    String url = Env.posts;

    final res = await http.get(
      Uri.parse(url),
    );

    dynamic postJson = json.decode(res.body);

    for (var i = 0; i < postJson.length; i++) {
      PostModel postModel = PostModel.fromJson(postJson[i]);
      postModelList.add(postModel);
    }

    return postModelList;
  }
}
