import 'package:http/http.dart' as http;

import 'dart:convert';
import '../enviroment/environment.dart';

import '../models/user_model.dart';

class UsersService {
  Future<List<UserModel>>? getUserList() async {
    List<UserModel> userModelList = [];
    String url = Env.users;

    final res = await http.get(
      Uri.parse(url),
    );

    dynamic userJson = json.decode(res.body);

    for (var i = 0; i < userJson.length; i++) {
      UserModel userModel = UserModel.fromJson(userJson[i]);
      userModelList.add(userModel);
    }

    return userModelList;
  }
}
