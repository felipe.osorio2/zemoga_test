import 'package:flutter/cupertino.dart';
import 'package:zemoga_test/core/models/user_model.dart';

import '../models/comment_model.dart';
import '../services/comment_service.dart';
import '../services/users_service.dart';

class PostDetailPageProvider extends ChangeNotifier {
  final userService = UsersService();
  final commentService = CommentService();

  late List<UserModel>? _userModelList = [];

  List<UserModel>? get userModelList => _userModelList;

  set userModelList(List<UserModel>? value) {
    _userModelList = value;
    notifyListeners();
  }

  late List<CommentModel>? _commentModelList = [];

  List<CommentModel>? get commentModelList => _commentModelList;

  set commentModelList(List<CommentModel>? value) {
    _commentModelList = value;
    notifyListeners();
  }

  Future<void> getUserList() async {
    List<UserModel>? postModelListFromService = await userService.getUserList();

    userModelList = postModelListFromService;

    notifyListeners();
  }

  Future<void> getUserComments(postID) async {
    List<CommentModel>? commentModelListFromService =
        await commentService.getCommentList(postID);

    commentModelList = commentModelListFromService;

    notifyListeners();
  }

  getUserListById(int id) {
    UserModel? userListById;

    for (var i = 0; i < userModelList!.length; i++) {
      if (userModelList![i].id == id) {
        userListById = userModelList![i];
      }
    }

    return userListById;
  }
}
