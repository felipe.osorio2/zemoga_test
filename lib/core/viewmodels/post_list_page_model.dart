import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zemoga_test/core/viewmodels/post_detail_page_model.dart';

import '../../preferences/preferences_user.dart';
import '../../ui/pages/post/post_detail_page.dart';
import '../models/comment_model.dart';
import '../models/post_model.dart';

import '../models/user_model.dart';
import '../services/post_service.dart';

class PostListPageProvider extends ChangeNotifier {
  final postService = PostService();
  final prefs = PreferenciasUsuario();

  bool _postReady = false;

  bool get postReady => _postReady;

  set postReady(bool value) {
    _postReady = value;
    notifyListeners();
  }

  late List<PostModel>? _postModelList = [];

  List<PostModel>? get postModelList => _postModelList;

  set postModelList(List<PostModel>? value) {
    _postModelList = value;
    notifyListeners();
  }

  late List<PostModel> _postFavoriteModelList = [];

  List<PostModel> get postFavoriteModelList => _postFavoriteModelList;

  set postFavoriteModelList(List<PostModel> value) {
    _postFavoriteModelList = value;
    notifyListeners();
  }

  Future<void> getPostList() async {
    if (prefs.postList.isEmpty) {
      List<PostModel>? postModelListFromService =
          await postService.getPostList();
      postModelList = postModelListFromService;
      prefs.postList = postModelToJson(postModelList!);
    } else {
      postModelList = postModelFromJson(prefs.postList);
    }

    if (_postFavoriteModelList.isNotEmpty) {
      for (var i = 0; i < _postModelList!.length; i++) {
        for (var j = 0; j < _postFavoriteModelList.length; j++) {
          if (_postModelList![i].id == _postFavoriteModelList[j].id) {
            _postModelList![i].favorite = true;
            PostModel prueba = postModelList![i];
            _postModelList!.remove(prueba);
            _postModelList!.insert(0, prueba);
          }
        }
      }
    }
    _postReady = true;

    notifyListeners();
  }

  void changeFavoriteTrue(PostModel postModel) {
    //set model on favorite on true and save on list of favorites
    postModel.favorite = true;
    _postFavoriteModelList.add(postModel);
    _postModelList!.remove(postModel);
    _postModelList!.insert(0, postModel);

    notifyListeners();
  }

  void changeFavoriteFalse(PostModel postMode) {
//set model on favorite on false and go lo favorite list to remove this element and change on postModelist the element to set on false
    postMode.favorite = false;
    for (var i = 0; i < _postFavoriteModelList.length; i++) {
      if (_postFavoriteModelList[i].id == postMode.id) {
        _postFavoriteModelList[i].favorite = false;
        _postFavoriteModelList.removeAt(i);
      }
    }
    for (var i = 0; i < postModelList!.length; i++) {
      if (postModelList![i].id == postMode.id) {
        postModelList![i].favorite = false;
        PostModel prueba = postModelList![i];
        _postModelList!.remove(prueba);
        _postModelList!.add(prueba);
      }
    }

    notifyListeners();
  }

  void cleanPost() {
    postModelList!.clear();

    notifyListeners();
  }

  navigationToPostListDetailPage(PostModel postModel, context) async {
    final userProvider =
        Provider.of<PostDetailPageProvider>(context, listen: false);
    UserModel user = userProvider.getUserListById(postModel.userId);
    await userProvider.getUserComments(postModel.id);
    List<CommentModel>? commentListbyPost = userProvider.commentModelList;
    // ignore: use_build_context_synchronously
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PostDetailPage(
            postModel: postModel,
            userModel: user,
            listCommentModel: commentListbyPost),
      ),
    );
  }
}
